from cx_Freeze import setup, Executable
import sys
import os
import numpy
#import pandas
#import xml.dom.minidom
#import glob

os.environ['TCL_LIBRARY'] = "C:\\Users\\rwinkler\\AppData\\Local\\Programs\\Python\\Python36\\tcl\\tcl8.6"
os.environ['TK_LIBRARY'] = "C:\\Users\\rwinkler\\AppData\\Local\\Programs\\Python\\Python36\\tcl\\tk8.6"

#adjust the file location, python folder name and username

files = {"include_files": ["C:\\Windows\\System32\\vcruntime140.dll", "C:\\Users\\rwinkler\\AppData\\Local\\Programs\\Python\\Python36\\DLLs\\tcl86t.dll", "C:\\Users\\rwinkler\\AppData\\Local\\Programs\\Python\\Python36\\DLLs\\tk86t.dll", "C:\\Users\\rwinkler\\AppData\\Local\\Programs\\Python\\Python36\\tcl\\tk8.6", "C:\\Users\\rwinkler\\AppData\\Local\\Programs\\Python\\Python36\\tcl\\tcl8.6"], "packages": ["tkinter","numpy"]}

#if sys.platform == 'win32':
#    base = "Win32GUI"
base = None

executables = [
    Executable('protyquant.py', base=base, icon='protyquant.ico')
]

setup(name='ProtyQuant',
      version = '1.0',
      description = 'Calculating the weighted Protein Presence (wPP) from pepXML files.',
      options = {"build_exe": files},
      executables = executables)
