#########################################################################################################
# Graphical user interface for comparative proteomics.
# 2020-05-11, Robert Winkler <robert.winkler@cinvestav.mx>
# License: GNU GPL v3
#########################################################################################################

from tkinter import *
from tkinter import filedialog
from tkinter.messagebox import *
import tkinter.messagebox as tkMessageBox
#from subprocess import call
import sys
import os
import xml.dom.minidom
import pandas as pd
import glob

# define functions

def help_usage():
	showinfo('Help',"Calculation of weighted Protein Presencence from pepXML files.\nThe pepXML(s) must contain peptide probabilities as estimated by PeptideProphet.\n ProtyQuant does not like space characters in the file names.")

def help_about():
	showinfo('About',"ProtyQuant 1.0 \nLicense: GPLv3 \nRobert Winkler, 2020\nrobert.winkler@cinvestav.mx")

def ready_info():
	showinfo('Ready',"Please look in the pepXML directory for results \n(.tsv and .uniprot.tsv)")

def check_OS():
	string_detected_OS = "ProtyQuant is running on " + sys.platform
	print(string_detected_OS)
	tkMessageBox.showinfo('Operating System', string_detected_OS)

def select_pepXML_path():
	pepxmlpath = filedialog.askdirectory()
	pepxmlpath_value.set(pepxmlpath)

def rreplace(s, old, new, occurrence):
	li = s.rsplit(old, occurrence)
	return new.join(li)

def select_fastafile():
	fastafile = filedialog.askopenfilename()
	fastafile_value.set(fastafile)

def run_analyses():
	print("Running the pipeline!")
	convert_pepXML_to_pipq()
	create_fasta_index()
	run_PIPQ()
	protein_report()
	sample_comparison()
	ready_info()

def create_fasta_index():

	# check, if labelling is chosen:

	lab1 = chk_protmap_short_state.get()
	lab2 = chk_protmap_long_state.get()
	lab3 = chk_protmap_url_state.get()
	lab4 = chk_compmap_short_state.get()
	lab5 = chk_compmap_long_state.get()
	lab6 = chk_compmap_url_state.get()
	labsum = lab1 + lab2 + lab3 + lab4 + lab5 + lab6

	#only create FASTA INDEX if needed for labelling, and not existing already for this FASTA DB
	if labsum != 0:

		input_fasta = fastafile_value.get()
		fasta_index = input_fasta + '.index'

		# FASTA INDEX
		if os.path.isfile(fasta_index):
			print ("Using existing FASTA index file: " + fasta_index + "\n")
		else:
			print ("FASTA index file " + fasta_index + " will be created. Please be patient.\n")

				#create fasta index file
			fastapanda = pd.DataFrame()

			filepath = input_fasta
			with open(filepath) as fp:
				line = fp.readline()
				cnt = 1
				while line:
					if ">" in line:
						fastaprot = line.replace(">","")
						#fastaprot = fastaprot.split(" ", 1)
						#fastaprot = fastaprot.split("OS", 1)
						if not ' ' in fastaprot:
							fastaprot = fastaprot + " " + "putative protein"
						fastaprot = re.split(" ", fastaprot, maxsplit=1)
						#print(fastaprot)
						fastaid = fastaprot[0]
						fastaid = fastaid.replace("\n","")
						#print(fastaid)
						proteinname = fastaprot[1]
						#print(proteinname)
						proteinname_short = re.split("OS=", proteinname)
						proteinname_short = proteinname_short[0]
						proteinname_short = proteinname_short.replace("\n","")
						#print(proteinname_short)
						#if not "\|" in fastaid:
						#	url = 'not publicly available'
						#if "\|" in fastaid:
						uniprotaccesion = re.split("\|", fastaid, maxsplit=2)
						url = 'http://www.uniprot.org/uniprot/' + uniprotaccesion[1]
						#print(url)
						fastapanda = fastapanda.append([[fastaid,proteinname_short,proteinname,url]], ignore_index=True)
						print("processing sequence " + str(cnt) + "\n")

					line = fp.readline()
					cnt += 1

			fastapanda.rename(columns={0:'ProtID',1:'short_name',2:'Uniprot_name',3:'Uniprot_URL'}, inplace=True)
			fastapanda.to_csv(fasta_index, sep="\t", index=False)

def convert_pepXML_to_pipq():

	infilepath = pepxmlpath.get()
	extension = pepxml_extension.get()
	if extension == 1:
		infilenames = infilepath + "/*" + '.pepXML'
	if extension == 2:
		infilenames = infilepath + "/*" + '.pepxml'
	if extension == 3:
		infilenames = infilepath + "/*" + '.pep.xml'

	for infile_name in glob.glob(infilenames):
		print("reading: " + infile_name)
		if extension == 1:
			outfile_name = rreplace(infile_name,".pepXML",".pipq", 1)
		if extension == 2:
			outfile_name = rreplace(infile_name,".pepxml",".pipq", 1)
		if extension == 3:
			outfile_name = rreplace(infile_name,".pep.xml",".pipq", 1)
		print(outfile_name)

		# test if conversion was already done
		if os.path.isfile(outfile_name):
			print ("pepXML already converted to PIPQ: " + outfile_name + "\n")
		else:
			doc = xml.dom.minidom.parse(infile_name)

			print("************************************************************\n")
			print("Converting pepXML file to PIPQ: " + infile_name + "\n")

			# Delete old pipq outfile with same name
			if os.path.isfile(outfile_name):
				os.remove(outfile_name)

			for n1 in doc.getElementsByTagName("search_hit"):
				with open(outfile_name, "a") as out_file:
					out_file.write("e " + n1.getAttribute("peptide") + "\n")
					print(n1.getAttribute("peptide"))
					out_file.write("r " + n1.getAttribute("protein") + "\n")
				for n2 in n1.childNodes:
					if n2.nodeName == "alternative_protein":
						with open(outfile_name, "a") as out_file:
							out_file.write("r " + n2.getAttribute("protein") + "\n")
					if n2.nodeName == "analysis_result":
						for n3 in n2.childNodes:
							if n3.nodeName == "peptideprophet_result":
								with open(outfile_name, "a") as out_file:
									out_file.write("p " + n3.getAttribute("probability") + "\n")

			print("PIPQ file: " + outfile_name + "\n")
			print("Ready!\n")
			print("************************************************************")

def run_PIPQ():

	infilepath = pepxmlpath.get()
	infilenames = infilepath + "/*" + '.pipq'

	for infile_name in glob.glob(infilenames):
		outfile_pi_name = rreplace(infile_name,".pipq",".pi",1)
		outfile_pq_name = rreplace(infile_name,".pipq",".pq",1)

		pipq_method_sel = pipq_algorithm.get()
		if pipq_method_sel == 1:
			pipq_method = "-s"
		if pipq_method_sel == 2:
			pipq_method = "-a"
		if pipq_method_sel == 3:
			pipq_method = "-lp"

		print("************************************************************\n")
		print("PIPQ analysis: " + infile_name + "\n")

		#Call external program, depending on operating system

		if sys.platform == "linux" or sys.platform == "linux2":
			pipq_call = os.getcwd() + '/PIPQ.x64 '
			#call([pipq_call, pipq_method," ",infile_name," ",outfile_pi_name," ",outfile_pq_name])
			command_string = str(pipq_call + pipq_method + " " + infile_name + " " + outfile_pi_name + " " + outfile_pq_name)
			print("command: " + command_string + "\n")
			os.system(command_string)

		if sys.platform == "win32":
			pipq_call = os.getcwd() + '/PIPQ.exe '
			#call([pipq_call, pipq_method," ",infile_name," ",outfile_pi_name," ",outfile_pq_name])
			command_string = str(pipq_call + pipq_method + " " + infile_name + " " + outfile_pi_name + " " + outfile_pq_name)
			print("command: " + command_string + "\n")
			os.system(command_string)

		print("PIPQ result files: \n")
		print("Protein interference:\t" + outfile_pi_name + "\n")
		print("Protein quantification:\t" + outfile_pq_name +  "\n")
		print("Ready!\n")
		print("************************************************************")

def protein_report():

	infilepath = pepxmlpath.get()
	infilenames = infilepath + "/*" + '.pipq'

	#set result filtering values
	minp = float(pval_prot.get())
	minq = float(quant_prot.get())

	for infile_name in glob.glob(infilenames):
		outfile_pi_name = rreplace(infile_name,".pipq",".pi",1)
		outfile_pq_name = rreplace(infile_name,".pipq",".pq",1)
		outfile_prot_tsv_name = rreplace(infile_name,".pipq",".prot.tsv",1)

		print("************************************************************\n")
		print("Creating protein report: " + infile_name + "\n")

		colpi=['id','p']
		colpq=['id','q']

		pi = pd.read_csv(outfile_pi_name, sep="\t", names=colpi, header=None)
		pq = pd.read_csv(outfile_pq_name, sep="\t", names=colpi, header=None)
		merged = pq.merge(pi, on='id', how='outer')
		merged = merged.fillna(0)

		merged.rename(columns={'id':'ProtID','p_x':'app','p_y':'Pr' }, inplace=True)

		#clean table
		merged = merged[merged['Pr'] >= minp]
		merged = merged[merged['app'] >= minq]

		print(merged)
		merged.to_csv(outfile_prot_tsv_name, sep="\t", index=False)

		print("\n")
		print("Protein report written to:\t" + outfile_prot_tsv_name +  "\n")
		print("Results filtered with a minium Probability (Pr) of " + str(minp) + " and a minimum abundance (app) of " + str(minq) + ".\n")
		print("Ready!\n")
		print("************************************************************")

		# check, if labelling is chosen:

		# Output options
		# shortenend protein name (0/1)
		print_shortname = chk_protmap_short_state.get()

		# full Uniprot name
		print_uniname = chk_protmap_long_state.get()

		# link to Uniprot URL (0/1)
		print_url = chk_protmap_url_state.get()

		labsum = print_shortname + print_uniname + print_url

		if labsum != 0:
			input_fasta = fastafile_value.get()
			fasta_index = input_fasta + '.index'
			input_table = outfile_prot_tsv_name
			output_table = rreplace(input_table,".tsv",".uniprot.tsv",1)

			print("************************************************************\n")
			print("Labelling of table " + input_table + " with FASTA DB " + input_fasta + " \n")

			fasta_table = pd.read_csv(fasta_index, sep="\t")

			in_table = pd.read_csv(input_table, sep="\t")

			merged = in_table.merge(fasta_table, on='ProtID')

			# drop un-selected columns

			if print_shortname == 0:
				merged.drop('short_name', axis=1, inplace=True)
			if print_uniname == 0:
				merged.drop('Uniprot_name', axis=1, inplace=True)
			if print_url == 0:
				merged.drop('Uniprot_URL', axis=1, inplace=True)

			print(merged)
			merged.to_csv(output_table, sep="\t", index=False)

			print("\n")
			print("Labeled table written to: " + output_table + "\n")
			print("FASTA index: " + fasta_index + "\n")
			print("Ready!\n")
			print("************************************************************")


def sample_comparison():

	infilepath = pepxmlpath.get()
	infilenames = infilepath + "/*" + '.pq'

	#set result filtering values
	minq = float(quant_compprot.get())

	# final outfile name
	outfile_name = infilepath + "/" + finalresultfile.get()

	#For debugging only
	print("out:" + outfile_name)

	print("************************************************************\n")
	print("Sample comparison: \n")

	colpi=['id','p']

	colcount = 1

	for fname in glob.glob(infilenames):
		print("reading: " + fname)
		try:
			merged
		except:
			merged = pd.read_csv(fname, sep="\t", names=colpi, header=None)
			merged.columns.values[colcount] = fname
		else:
			colcount = colcount + 1
			q2 = pd.read_csv(fname, sep="\t", names=colpi, header=None)
			merged = merged.merge(q2, on='id', how='outer')
			merged.columns.values[colcount] = fname

	merged = merged.fillna(0)

	merged.rename(columns={'id':'ProtID'}, inplace=True)

	#clean table
	merged = merged[merged.iloc[:,1:].values >= minq]
	merged = merged.drop_duplicates()
	print(merged)

	merged.to_csv(outfile_name, sep="\t", index=False)

	print("Comparison of Weighted Protein Presence written to :\t" + outfile_name + "\n")
	print("Results filtered with a minimum weighted abundance of " + str(minq) + ".\n")
	print("Ready!\n")
	print("************************************************************")

	# check, if labelling is chosen:

	# Output options
	# shortenend protein name (0/1)
	print_shortname = chk_compmap_short_state.get()

	# full Uniprot name
	print_uniname = chk_compmap_long_state.get()

	# link to Uniprot URL (0/1)
	print_url = chk_compmap_url_state.get()

	labsum = print_shortname + print_uniname + print_url

	if labsum != 0:
		input_fasta = fastafile_value.get()
		fasta_index = input_fasta + '.index'
		input_table = outfile_name
		output_table = rreplace(input_table,".tsv",".uniprot.tsv",1)

		print("************************************************************\n")
		print("Labelling of table " + input_table + " with FASTA DB " + input_fasta + " \n")

		fasta_table = pd.read_csv(fasta_index, sep="\t")

		in_table = pd.read_csv(input_table, sep="\t")

		merged = in_table.merge(fasta_table, on='ProtID')

		# drop un-selected columns

		if print_shortname == 0:
			merged.drop('short_name', axis=1, inplace=True)
		if print_uniname == 0:
			merged.drop('Uniprot_name', axis=1, inplace=True)
		if print_url == 0:
			merged.drop('Uniprot_URL', axis=1, inplace=True)

		print(merged)
		merged.to_csv(output_table, sep="\t", index=False)

		print("\n")
		print("Labeled table written to: " + output_table + "\n")
		print("FASTA index: " + fasta_index + "\n")
		print("Ready!\n")
		print("************************************************************")

# GUI main window

root=Tk()
root.title("ProtyQuant 1.0")

# graphical user interface

features=Frame(root)
features.pack(side=LEFT)

# Space
header_label = Label(features, text="    ")
header_label.grid(row=0, column=0, sticky=W)
header_label = Label(features, text="    ")
header_label.grid(row=0, column=9, sticky=W)

# select directory of pepXML files
header_label = Label(features, text="1. LOAD pepXMLs")
header_label.grid(row=1, column=1, sticky=W)

action_button=Button(features, text="Select pepXML directory", command=select_pepXML_path)
action_button.grid(row=1, column=2)

pepxmlpath_value=StringVar()
pepxmlpath_value.set("PLEASE SELECT THE PATH OF PEPXML FILES")
#pepxmlpath_value.set("/home/rob/Nextcloud/DATA/USP48_yeast/pepXML-results")

pepxmlpath = Entry(features, textvariable=pepxmlpath_value, width=50)
pepxmlpath.grid(row=1, column=3, columnspan=3)

# Radiobutton for seleciton of pepXML extension
pepxml_extension = IntVar()
rad1p = Radiobutton(features,text='.pepXML', value=1, variable=pepxml_extension)
rad1p.grid(row=1, column=6)
rad2p = Radiobutton(features,text='.pepxml', value=2, variable=pepxml_extension)
rad2p.grid(row=1, column=7)
rad3p = Radiobutton(features,text='.pep.xml', value=3, variable=pepxml_extension)
rad3p.grid(row=1, column=8)
pepxml_extension.set(3)

# Space
header_label = Label(features, text=" ")
header_label.grid(row=2, column=1, sticky=W)

# select PIPQ algorithm
header_label = Label(features, text="2. PIPQ analysis")
header_label.grid(row=3, column=1, sticky=W)

header_label = Label(features, text="PIPQ algorithm:")
header_label.grid(row=3, column=2, sticky=W)
pipq_algorithm = IntVar()
rad1a = Radiobutton(features,text='-s   Multiple Counting algorithm', value=1, variable=pipq_algorithm)
rad1a.grid(row=4, column=2, sticky=W)
rad2a = Radiobutton(features,text='-a   Equal Division algorithm', value=2, variable=pipq_algorithm)
rad2a.grid(row=5, column=2, sticky=W)
rad3a = Radiobutton(features,text='-lp  Linear Programming Model algorithm', value=3, variable=pipq_algorithm)
rad3a.grid(row=6, column=2, sticky=W)
pipq_algorithm.set(1)

# Space
header_label = Label(features, text=" ")
header_label.grid(row=7, column=1, sticky=W)

# Protein report
header_label = Label(features, text="3. Protein report")
header_label.grid(row=8, column=1, sticky=W)

header_label = Label(features, text="Filter:")
header_label.grid(row=8, column=2, sticky=W)

header_label = Label(features, text="min. quant. [app] ")
header_label.grid(row=8, column=2, sticky=E)

quant_prot_value=DoubleVar()
quant_prot_value.set(1)
quant_prot = Entry(features, textvariable=quant_prot_value)
quant_prot.grid(row=8, column=3, sticky=W)

header_label = Label(features, text="   min. Prob. [Pr] ")
header_label.grid(row=8, column=4, sticky=E)

pval_prot_value=DoubleVar()
pval_prot_value.set(0.05)
pval_prot = Entry(features, textvariable=pval_prot_value)
pval_prot.grid(row=8, column=5, sticky=W)

# Space
header_label = Label(features, text=" ")
header_label.grid(row=9, column=1, sticky=W)

# Checkbox annotation
header_label = Label(features, text="Label protein table:")
header_label.grid(row=10, column=2, sticky=W)

chk_protmap_short_state = IntVar()
chk_protmap_short_state.set(1)
protmap_short = Checkbutton(features, text='Short protein name', var=chk_protmap_short_state)
protmap_short.grid(row=10, column=3, sticky=W)

chk_protmap_long_state = IntVar()
chk_protmap_long_state.set(1)
protmap_long = Checkbutton(features, text='Uniprot name', var=chk_protmap_long_state)
protmap_long.grid(row=10, column=5, sticky=W)

chk_protmap_url_state = IntVar()
chk_protmap_url_state.set(1)
protmap_url = Checkbutton(features, text='URL to Uniprot', var=chk_protmap_url_state)
protmap_url.grid(row=10, column=6, sticky=W)

# Space
header_label = Label(features, text=" ")
header_label.grid(row=11, column=1, sticky=W)

# Sample comparison
header_label = Label(features, text="4. Sample comparison")
header_label.grid(row=12, column=1, sticky=W)

header_label = Label(features, text="Filter:")
header_label.grid(row=12, column=2, sticky=W)

header_label = Label(features, text="min. quant. [app] ")
header_label.grid(row=12, column=2, sticky=E)

quant_compprot_value=DoubleVar()
quant_compprot_value.set(1)
quant_compprot = Entry(features, textvariable=quant_compprot_value)
quant_compprot.grid(row=12, column=3, sticky=W)

# Space
header_label = Label(features, text=" ")
header_label.grid(row=13, column=1, sticky=W)

# Checkbox annotation
header_label = Label(features, text="Label comparison table:")
header_label.grid(row=14, column=2, sticky=W)

chk_compmap_short_state = IntVar()
chk_compmap_short_state.set(1)
compmap_short = Checkbutton(features, text='Short protein name', var=chk_compmap_short_state)
compmap_short.grid(row=14, column=3, sticky=W)

chk_compmap_long_state = IntVar()
chk_compmap_long_state.set(1)
compmap_long = Checkbutton(features, text='Uniprot name', var=chk_compmap_long_state)
compmap_long.grid(row=14, column=5, sticky=W)

chk_compmap_url_state = IntVar()
chk_compmap_url_state.set(1)
compmap_url = Checkbutton(features, text='URL to Uniprot', var=chk_compmap_url_state)
compmap_url.grid(row=14, column=6, sticky=W)

# Space
header_label = Label(features, text=" ")
header_label.grid(row=15, column=1, sticky=W)

# Final result file name
header_label = Label(features, text="Comparison result file: ")
header_label.grid(row=16, column=2, sticky=W)

finalresultfile_value=StringVar()
finalresultfile_value.set('sample_comparison.tsv')
finalresultfile = Entry(features, textvariable=finalresultfile_value)
finalresultfile.grid(row=16, column=3, sticky=W)

# Space
header_label = Label(features, text=" ")
header_label.grid(row=20, column=1, sticky=W)

# select FASTA DB file
header_label = Label(features, text="FASTA DB for annotation")
header_label.grid(row=23, column=1, sticky=W)

action_button=Button(features, text="Select FASTA database", command=select_fastafile)
action_button.grid(row=23, column=2)

fastafile_value=StringVar()
fastafile_value.set("PLEASE SELECT THE .FASTA/FAS SEARCH DATABASE")
#fastafile_value.set("/home/rob/Nextcloud/DATA/USP48_yeast/ups1-ups2-sequences.fasta")
fastafile_output = Entry(features, textvariable=fastafile_value, width=50)
fastafile_output.grid(row=23, column=3, columnspan=3)

# Space
header_label = Label(features, text=" ")
header_label.grid(row=30, column=1, sticky=W)

action_button=Button(features, text="RUN ALL", command=run_analyses)
action_button.grid(row=31, column=1)

header_label = Label(features, text=" ")
header_label.grid(row=32, column=1, sticky=W)

# create a menu

menu = Menu(root)
root.config(menu=menu)

filemenu = Menu(menu)
menu.add_cascade(label="File", menu=filemenu)
filemenu.add_command(label="Exit", command=menu.quit)

optionmenu = Menu(menu)
menu.add_cascade(label="Options", menu=optionmenu)
optionmenu.add_command(label="Check Operating System", command=check_OS)

helpmenu = Menu(menu)
menu.add_cascade(label="Help", menu=helpmenu)
helpmenu.add_command(label="Usage", command=help_usage)
helpmenu.add_command(label="About...", command=help_about)

mainloop()
