# Build protyquant image

docker build -t protyquant .

# Publish on hub.docker.com

docker login --username=robertmp64

docker tag protyquant:latest robertmp64/protyquant

docker push robertmp64/protyquant:latest


