#########################################################################################################
# This script merges the information of pi and pq files
# 2018-06-11, Robert Winkler <robert.winkler@cinvestav.mx>
# License: GNU GPL v3
#########################################################################################################

import sys
import os
import pandas as pd

def print_usage():
	print("usage:")
	print("\tpython3 protein_report.py input_pipq_file")

if len(sys.argv) != 2:
	print_usage()
	exit(1)

infile_name = str(sys.argv[1])
outfile_pi_name = infile_name.replace("pipq","pi")
outfile_pq_name = infile_name.replace("pipq","pq")
outfile_prot_tsv_name = infile_name.replace("pipq","prot.tsv")

#set result filtering values
minp = 0.05
minq = 3

print("************************************************************\n")
print("Creating protein report: " + infile_name + "\n")

colpi=['id','p']
colpq=['id','q']

pi = pd.read_csv(outfile_pi_name, sep="\t", names=colpi, header=None)
pq = pd.read_csv(outfile_pq_name, sep="\t", names=colpi, header=None)
merged = pq.merge(pi, on='id', how='outer')
merged = merged.fillna(0)

merged.rename(columns={'id':'ProtID','p_x':'app','p_y':'Pr' }, inplace=True)

#clean table
merged = merged[merged['Pr'] >= minp]
merged = merged[merged['app'] >= minq]

print(merged)
merged.to_csv(outfile_prot_tsv_name, sep="\t", index=False)

print("\n")
print("Protein report written to:\t" + outfile_prot_tsv_name +  "\n")
print("Results filtered with a minium Prue of " + str(minp) + " and a minimum weighted abundance of " + str(minq) + ".\n")
print("Ready!\n")
print("************************************************************")
