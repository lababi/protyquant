#########################################################################################################
# This script loads .pq files to compare weighted protein presence.
# 2018-06-11, Robert Winkler <robert.winkler@cinvestav.mx>
# License: GNU GPL v3
#########################################################################################################

import sys
import os
import pandas as pd
import re

def print_usage():
	print("usage:")
	print("\tpython3 label_table_with_protein_names.py input-table input-fasta")

if len(sys.argv) != 3:
	print_usage()
	exit(1)

input_table = str(sys.argv[1])
input_fasta = str(sys.argv[2])
fasta_index = input_fasta + '.index'
if "tsv" in input_table:
	output_table = input_table.replace("tsv","uniprot.tsv")
if "txt" in input_table:
	output_table = input_table.replace("txt","uniprot.tsv")

# Output options
# shortenend protein name (0/1)
print_shortname = 1

# full Uniprot name
print_uniname = 0

# link to Uniprot URL (0/1)
print_url = 1

print("************************************************************\n")
print("Labelling of table " + input_table + " with FASTA DB " + input_fasta + " \n")

# FASTA INDEX
if os.path.isfile(fasta_index):
	print ("Using existing FASTA index file: " + fasta_index + "\n")
else:
	print ("FASTA index file " + fasta_index + " will be created. Please be patient.\n")

		#create fasta index file
	fastapanda = pd.DataFrame()

	filepath = input_fasta
	with open(filepath) as fp:
		line = fp.readline()
		cnt = 1
		while line:
			if ">" in line:
				fastaprot = line.replace(">","")
				#fastaprot = fastaprot.split(" ", 1)
				#fastaprot = fastaprot.split("OS", 1)
				fastaprot = re.split(" ", fastaprot, maxsplit=1)
				fastaid = fastaprot[0]
				#print(fastaid)
				proteinname = fastaprot[1]
				#print(proteinname)
				proteinname_short = re.split("OS=", proteinname)
				proteinname_short = proteinname_short[0]
				proteinname_short = proteinname_short.replace("\n","")
				#print(proteinname_short)
				uniprotaccesion = re.split("\|", fastaid, maxsplit=2)
				url = 'http://www.uniprot.org/uniprot/' + uniprotaccesion[1]
				fastapanda = fastapanda.append([[fastaid,proteinname_short,proteinname,url]], ignore_index=True)

			line = fp.readline()
			cnt += 1

	fastapanda.rename(columns={0:'ProtID',1:'short_name',2:'Uniprot_name',3:'Uniprot_URL'}, inplace=True)
	fastapanda.to_csv(fasta_index, sep="\t", index=False)

in_table = pd.read_csv(input_table, sep="\t")

#following line makes crux pipeline percolator and spectral counts compatible with mapping
in_table.rename(columns={'ProteinId':'ProtID','protein id':'ProtID'}, inplace=True)

fasta_table = pd.read_csv(fasta_index, sep="\t")

merged = in_table.merge(fasta_table, on='ProtID')

# drop un-selected columns

if print_shortname == 0:
	merged.drop('short_name', axis=1, inplace=True)
if print_uniname == 0:
	merged.drop('Uniprot_name', axis=1, inplace=True)
if print_url == 0:
	merged.drop('Uniprot_URL', axis=1, inplace=True)

print(merged)
merged.to_csv(output_table, sep="\t", index=False)

print("\n")
print("Labeled table written to: " + output_table + "\n")
print("FASTA index: " + fasta_index + "\n")
print("Ready!\n")
print("************************************************************")
