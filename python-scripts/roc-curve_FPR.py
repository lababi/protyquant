#########################################################################################################
# This script performs a ROC analysis
# 2018-06-11, Robert Winkler <robert.winkler@cinvestav.mx>
# License: GNU GPL v3
#########################################################################################################

import sys
import os
import pandas as pd
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
import matplotlib
#matplotlib.use('PDF')
import numpy as np
import random
import math

def print_usage():
	print("usage:")
	print("\tpython3 roc-curve_FPR.py input.prot.tsv_result_file")

if len(sys.argv) != 2:
	print_usage()
	exit(1)

def first(the_iterable, condition = lambda x: True):
	for i in the_iterable:
		if condition(i):
			return i

infile_name = str(sys.argv[1])

outfile_name = infile_name.replace(".prot.tsv",".")
outfile_FPR = outfile_name + "prot.FPR.tsv"
outfile_FPR_table = outfile_name + "FPR.table.csv"
outfile_pdf = outfile_name + "ROC.pdf"
outfile_ROC_summary = "ROC_summary.csv"

prot_table = pd.read_csv(infile_name, sep="\t", header=None)

input_IDs = pd.read_csv(infile_name, sep="\t", usecols=['ProtID'], squeeze=True)
input_Prs = pd.read_csv(infile_name, sep="\t", usecols=['Pr'])
input_apps = pd.read_csv(infile_name, sep="\t", usecols=['app'])

decoy_index = input_IDs.str.contains('DECOY', regex=False)
decoy_index = decoy_index * 1
actual = 1 - (decoy_index * 1)

FPR_list = ['FPR']
FPs = 0
counter = 1

for i in range(0, len(decoy_index)):
	if decoy_index[i] == 1:
		FPs = FPs + 1
	FPR = 100 * FPs / counter
	FPR_list.append(FPR)
	counter = counter + 1

# Add continuous FPR to the protein table

prot_table['FPR'] = FPR_list
prot_table.to_csv(outfile_FPR, sep="\t", index=False, header=None)

print("************************************************************")
print("False Positive Rates (FPRs) added to:\t" + outfile_FPR +  "\n")
print("************************************************************")

# Create FPR overview table

FPR_list_sort = sorted(FPR_list[1:]) 
FPR_list_max = math.floor(max(FPR_list_sort))+1

cutoff_list = list(range(1,FPR_list_max))
protein_list = []

for FPR_cutoff in range(1,FPR_list_max):
	FPR_idx = next(i for i,v in enumerate(FPR_list_sort) if v > FPR_cutoff)
	protein_list.append(FPR_idx)

FPR_table = pd.DataFrame({'FPR [%] <=' : cutoff_list, 'proteins' : protein_list})

FPR_table.to_csv(outfile_FPR_table, sep="\t", index=False, header=True)

print("************************************************************")
print("False Positive Rate (FPR) table written to:\t" + outfile_FPR_table +  "\n")
print("************************************************************")

# Calculate Receiver Operating Curve (ROC) and the Area Under the Curve (AUC)
# using the prediction probabilities as input for classifier
# The functions of the python SCIKIT library https://scikit-learn.org are used

FPR_list = FPR_list[1:]

predictions_Pr = input_Prs

plt.figure(1, figsize=(4,9))
plt.subplot(311)
plt.title('Receiver Operating Characteristic')

false_positive_rate, true_positive_rate, thresholds = roc_curve(actual, predictions_Pr)

roc_auc = auc(false_positive_rate, true_positive_rate)

plt.plot(false_positive_rate, true_positive_rate, 'b', label='AUC (Pr) = %0.2f'% roc_auc)
plt.plot([0,1],[0,1],'r--', label='AUC (random)')
plt.axvline(x=0.01, color = 'g', label='FPR 0.01' , ls='--')
plt.axvline(x=0.05, color = 'g', label='FPR 0.05' , ls='--')
plt.axvline(x=0.10, color = 'g', label='FPR 0.10' , ls='--')
plt.legend(loc='lower right')
plt.xlim([0,1])
plt.ylim([0,1])
plt.gca().set_aspect('equal', adjustable='box')

fig_size = plt.gcf().get_size_inches() 
sizefactor = 1.5 
plt.gcf().set_size_inches(sizefactor * fig_size)

plt.xticks(np.arange(0,1.1,0.1))
plt.yticks(np.arange(0,1.1,0.1))
plt.grid(True)
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')

# Plot the correlation between accumulated peptide probability (app), Probability (Pr) and false positive rate (FPR)

plt.subplot(313)
plt.title('APP Graph')

prot_table.sort_values(float(prot_table.columns[2]))

#FPR_list = FPR_list[1:]
FPR_list_1 = [x / 100 for x in FPR_list]
prot_list_elements = list(range(0,len(input_apps)))
prot_list_elements_prop = [x / len(input_apps) for x in prot_list_elements]

plt.plot(input_apps,FPR_list_1, label='FPR')
plt.plot(input_apps,input_Prs, label='Pr')
plt.plot(input_apps,prot_list_elements_prop, label='proteins')
plt.axhline(y=0.01, color = 'g', label='FPR 0.01' , ls='--')
plt.axhline(y=0.05, color = 'g', label='FPR 0.05' , ls='--')
plt.axhline(y=0.10, color = 'g', label='FPR 0.10' , ls='--')
plt.yticks(np.arange(0,1.1,0.1))
plt.grid(True)
plt.legend(loc='center right')

#app_max = max(input_apps['app'].tolist())
#app_min = min(input_apps['app'].tolist())
#plt.xlim([app_min,app_max])
plt.xlim([0,20])

plt.xlabel('accumulated peptide probability [app]')
plt.ylabel('FPR, Pr, proteins [1]')

plt.savefig(outfile_pdf)

print("************************************************************")
print("Receiver Operating Characteristic (ROC) graphs saved to:\t" + outfile_pdf +  "\n")
print("************************************************************")

# Write AUC into file ROC_summary.csv

import csv   
fields=[roc_auc]
with open(outfile_ROC_summary, 'a') as froc:
    writer = csv.writer(froc)
    writer.writerow(fields)

