#########################################################################################################
# Original version created on Oct 7, 2012:
# https://code.google.com/archive/p/protein-inference/
# see He Z, Huang T, Liu X, Zhu P, Teng B, Deng S. Protein inference: A protein
# quantification perspective. Comput Biol Chem. 2016 Aug;63:21-29. doi:
# 10.1016/j.compbiolchem.2016.02.006. Epub 2016 Feb 13. PubMed PMID: 26935399.
# 2018-06-08, writing to .pipq file, Robert Winkler <robert.winkler@cinvestav.mx>
# License: GNU GPL v3
#########################################################################################################

import sys
import os
import xml.dom.minidom

def print_usage():
	print("usage:")
	print("\tpython3 pepxml_to_pipq.py input_pepxml_file")

if len(sys.argv) != 2:
	print_usage()
	exit(1)

infile_name = str(sys.argv[1])

if ".pepXML" in infile_name:
	outfile_name = infile_name.replace(".pepXML",".pipq")
if ".pepxml" in infile_name:
	outfile_name = infile_name.replace(".pepxml",".pipq")
if ".pep.xml" in infile_name:
	outfile_name = infile_name.replace(".pep.xml",".pipq")

#test if conversion was already done
if os.path.isfile(outfile_name):
	print ("pepXML already converted to PIPQ: " + outfile_name + "\n")
else:
	doc = xml.dom.minidom.parse(infile_name)

	print("************************************************************\n")
	print("Converting pepXML file to PIPQ: " + infile_name + "\n")

	# Delete old pipq outfile with same name
	if os.path.isfile(outfile_name):
	    os.remove(outfile_name)

	for n1 in doc.getElementsByTagName("search_hit"):
		with open(outfile_name, "a") as out_file:
			out_file.write("e " + n1.getAttribute("peptide") + "\n")
			out_file.write("r " + n1.getAttribute("protein") + "\n")
		for n2 in n1.childNodes:
			if n2.nodeName == "alternative_protein":
				with open(outfile_name, "a") as out_file:
					out_file.write("r " + n2.getAttribute("protein") + "\n")
			if n2.nodeName == "analysis_result":
				for n3 in n2.childNodes:
					if n3.nodeName == "peptideprophet_result":
						with open(outfile_name, "a") as out_file:
							out_file.write("p " + n3.getAttribute("probability") + "\n")

	print("PIPQ file: " + outfile_name + "\n")
	print("Ready!\n")
	print("************************************************************")
