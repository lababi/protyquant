#########################################################################################################
# This script loads .pq files to compare weighted protein presence.
# 2018-06-11, Robert Winkler <robert.winkler@cinvestav.mx>
# License: GNU GPL v3
#########################################################################################################

import sys
import os
import pandas as pd
import glob


def print_usage():
	print("usage:")
	print("\tpython3 compare samples.py OUTFILE_NAME")

if len(sys.argv) != 2:
	print_usage()
	exit(1)

outfile_name = str(sys.argv[1])

#set result filtering values
minq = 1

#For debugging only
print("out:" + outfile_name)

print("************************************************************\n")
print("Sample comparison: \n")

colpi=['id','p']

path = "*.pq"
colcount = 1

for fname in glob.glob(path):
	print("reading: " + fname)
	try:
		merged
	except:
		merged = pd.read_csv(fname, sep="\t", names=colpi, header=None)
		merged.columns.values[colcount] = fname
	else:
		colcount = colcount + 1
		q2 = pd.read_csv(fname, sep="\t", names=colpi, header=None)
		merged = merged.merge(q2, on='id', how='outer')
		merged.columns.values[colcount] = fname

merged = merged.fillna(0)

merged.rename(columns={'id':'ProtID'}, inplace=True)

#clean table
merged = merged[merged.iloc[:,1:].values >= minq]
merged = merged.drop_duplicates()
print(merged)

merged.to_csv(outfile_name, sep="\t", index=False)

print("Comparison of Weighted Protein Presence written to :\t" + outfile_name + "\n")
print("Results filtered with a minimum weighted abundance of " + str(minq) + ".\n")
print("Ready!\n")
print("************************************************************")
