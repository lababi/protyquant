#########################################################################################################
# Original version created on Oct 7, 2012:
# https://code.google.com/archive/p/protein-inference/
# see He Z, Huang T, Liu X, Zhu P, Teng B, Deng S. Protein inference: A protein
# quantification perspective. Comput Biol Chem. 2016 Aug;63:21-29. doi:
# 10.1016/j.compbiolchem.2016.02.006. Epub 2016 Feb 13. PubMed PMID: 26935399.
# 2018-06-08, writing to .pipq file, Robert Winkler <robert.winkler@cinvestav.mx>
# 2018-12-03, This script only extracts peptides containing cysteines with C12/C13 acrylamide labeled modifications.
# License: GNU GPL v3
#########################################################################################################

import sys
import os
import xml.dom.minidom

def print_usage():
	print("usage:")
	print("\tpython3 pepxml_C12-C13_to_pipq.py input_pepxml_file")

if len(sys.argv) != 2:
	print_usage()
	exit(1)

infile_name = str(sys.argv[1])

if ".pepXML" in infile_name:
	outfile_name = infile_name.replace(".pepXML",".pipq")
if ".pepxml" in infile_name:
	outfile_name = infile_name.replace(".pepxml",".pipq")
if ".pep.xml" in infile_name:
	outfile_name = infile_name.replace(".pep.xml",".pipq")

C12_outfile_name = outfile_name.replace(".pipq","_C12.pipq")
C13_outfile_name = outfile_name.replace(".pipq","_C13.pipq")
TSV_outfile_name = infile_name.replace(".pep.xml",".cysteines.tsv")

#test if conversion was already done
if os.path.isfile(C12_outfile_name):
	print ("pepXML already converted to PIPQ: " + C12_outfile_name + "\n")
if os.path.isfile(C13_outfile_name):
	print ("pepXML already converted to PIPQ: " + C13_outfile_name + "\n")
else:
	doc = xml.dom.minidom.parse(infile_name)

	print("************************************************************\n")
	print("Converting pepXML file to PIPQ (separate for C12 and C13 modifications): " + infile_name + "\n")

	PSM_hits = 0
	PSM_hits_PP = 0
	total_C_hits = 0
	total_C_hits_PP = 0
	C12_hits = 0
	C12_PP = 0
	C13_hits = 0
	C13_PP = 0

	for n1 in doc.getElementsByTagName("search_hit"):
		print("\n")
		print("************************************************************")
		print("PSM HIT")
		print("************************************************************")
		PSM_hits = PSM_hits + 1
		modification_index = 0
		export_string = "empty \n"
		with open(outfile_name, "a") as out_file:
			peptide = n1.getAttribute("peptide")
			print("peptide: " + peptide)
			if "C" in peptide:
				print("contains C")
			print("protein: " + n1.getAttribute("protein"))
			out_file.write("e " + n1.getAttribute("peptide") + "\n")
			out_file.write("r " + n1.getAttribute("protein") + "\n")

		for n2 in n1.childNodes:
			if n2.nodeName == "modification_info":
				modpeptide = n2.getAttribute("modified_peptide")
				print("modified peptide: " + modpeptide)
				if "C[174]" in modpeptide:
					C12_hits = C12_hits + 1
					total_C_hits = total_C_hits + 1
					with open(C12_outfile_name, "a") as out_C12file:
						out_C12file.write("e " + n1.getAttribute("peptide") + "\n")
						out_C12file.write("r " + n1.getAttribute("protein") + "\n")
					export_string = n1.getAttribute("peptide") + "\t" + n1.getAttribute("protein") + "\t" + modpeptide + "\t"  + "C12" + "\t"
					print("C12:" + modpeptide)
					modification_index = 12
				if "C[177]" in modpeptide:
					C13_hits = C13_hits + 1
					total_C_hits = total_C_hits + 1
					with open(C13_outfile_name, "a") as out_C13file:
						out_C13file.write("e " + n1.getAttribute("peptide") + "\n")
						out_C13file.write("r " + n1.getAttribute("protein") + "\n")
					export_string = n1.getAttribute("peptide") + "\t" + n1.getAttribute("protein") + "\t" + modpeptide + "\t"  + "C13" + "\t"
					print("C13:" + modpeptide)
					modification_index = 13
				print("C modification index (0=none, 12=C12, 13=C13): " + str(modification_index))
			if n2.nodeName == "alternative_protein":
				with open(outfile_name, "a") as out_file:
					out_file.write("r " + n2.getAttribute("protein") + "\n")
				print("alternative protein: " + n2.getAttribute("protein"))
				if modification_index == 12:
					with open(C12_outfile_name, "a") as out_C12file:
						out_C12file.write("r " + n2.getAttribute("protein") + "\n")
				if modification_index == 13:
					with open(C13_outfile_name, "a") as out_C13file:
						out_C13file.write("r " + n2.getAttribute("protein") + "\n")
			if n2.nodeName == "analysis_result":
				for n3 in n2.childNodes:
					if n3.nodeName == "peptideprophet_result":
							PSM_hits_PP = PSM_hits_PP + 1
							pep_prop = n3.getAttribute("probability")
							with open(outfile_name, "a") as out_file:
								out_file.write("p " + n3.getAttribute("probability") + "\n")
							if modification_index == 12:
								C12_PP = C12_PP + 1
								total_C_hits_PP = total_C_hits_PP + 1
								with open(C12_outfile_name, "a") as out_C12file:
									out_C12file.write("p " + n3.getAttribute("probability") + "\n")
								with open(TSV_outfile_name, "a") as out_table:
									out_table.write(export_string + n3.getAttribute("probability") + "\n")
							if modification_index == 13:
								C13_PP = C13_PP + 1
								total_C_hits_PP = total_C_hits_PP + 1
								with open(C13_outfile_name, "a") as out_C13file:
									out_C13file.write("p " + n3.getAttribute("probability") + "\n")
								with open(TSV_outfile_name, "a") as out_table:
									out_table.write(export_string + n3.getAttribute("probability") + "\n")
							print("peptide probability: " + pep_prop)

	print(export_string)
	print("\n")
	print("************************************************************")
	print("PSM hits :" + str(PSM_hits))
	print("PSM hits (PP validated): " + str(PSM_hits_PP))
	print("PSM hits with C: " + str(total_C_hits))
	print("PSM hits with C (PP validated): " + str(total_C_hits_PP))
	print("PSM hits with C12: " + str(C12_hits))
	print("PSM hits with C12 (PP validated): " + str(C12_PP))
	print("PSM hits with C13: " + str(C13_hits))
	print("PSM hits with C13 (PP validated): " + str(C13_PP))
	print("PIPQ files: " + outfile_name + "," + C12_outfile_name + "," + C13_outfile_name + "\n")
	print("Ready!")
	print("************************************************************")
