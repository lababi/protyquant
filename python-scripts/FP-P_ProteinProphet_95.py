#########################################################################################################
# This script counts target (TP) and decoy (FP) hits a a defined probability (default 0.95)
# 2020-05-20, Robert Winkler <robert.winkler@cinvestav.mx>
# License: GNU GPL v3
#########################################################################################################

import sys
import os
import pandas as pd
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
import matplotlib
#matplotlib.use('PDF')
import numpy as np
import random
import math

def print_usage():
	print("usage:")
	print("\tpython3 FP-P_ProteinProphet95.py input.prot.tsv_result_file")

if len(sys.argv) != 2:
	print_usage()
	exit(1)

def first(the_iterable, condition = lambda x: True):
	for i in the_iterable:
		if condition(i):
			return i

infile_name = str(sys.argv[1])
outfile_Pr_summary = "Pr_summary.csv"

prot_table = pd.read_csv(infile_name, sep="\t", header=None)

input_IDs = pd.read_csv(infile_name, sep="\t", usecols=['protein_name'], squeeze=True)
input_Prs = pd.read_csv(infile_name, sep="\t", usecols=['probability'])
#input_apps = pd.read_csv(infile_name, sep="\t", usecols=['total_number_peptides'])

decoy_index = input_IDs.str.contains('DECOY', regex=False)
decoy_index = decoy_index * 1
actual = 1 - (decoy_index * 1)
#print(decoy_index)
#print(input_Prs)

#TP_list = ['TP']
#FP_list = ['FP']
TP = 0
FP = 0
counter = 1

for i in range(0, len(input_Prs)):
	Pr_input_float = input_Prs['probability'][i]

	if Pr_input_float >= 0.95:
		if decoy_index[i] == 1:
			FP = FP + 1
		if decoy_index[i] == 0:
			TP = TP + 1
			
	#FPR = 100 * FP / (TP + FP)
	#FPR_list.append(FPR)
	counter = counter + 1

# Write Pr into file Pr_summary.csv

import csv   
fields=[TP,FP]
with open(outfile_Pr_summary, 'a') as froc:
    writer = csv.writer(froc)
    writer.writerow(fields)

