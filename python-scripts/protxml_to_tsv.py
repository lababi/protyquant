#########################################################################################################
# 2018-11-14, converting protXML to .tsv file, Robert Winkler <robert.winkler@cinvestav.mx>
# License: GNU GPL v3
#########################################################################################################

import sys
import os
import xml.dom.minidom

def print_usage():
	print("usage:")
	print("\tpython3 protxml_to_tsv.py input_protxml_file")

if len(sys.argv) != 2:
	print_usage()
	exit(1)

infile_name = str(sys.argv[1])

if ".protXML" in infile_name:
	outfile_name = infile_name.replace(".protXML",".protXML.tsv")
if ".protxml" in infile_name:
	outfile_name = infile_name.replace(".protxml",".protXML.tsv")
if ".prot.xml" in infile_name:
	outfile_name = infile_name.replace(".prot.xml",".protXML.tsv")

doc = xml.dom.minidom.parse(infile_name)

print("************************************************************\n")
print("Converting protXML file to tsv: " + infile_name + "\n")

	# Delete old tsv outfile with same name
if os.path.isfile(outfile_name):
	os.remove(outfile_name)
	
with open(outfile_name, "a") as out_file:
	out_file.write("probability" + "\t" + "protein_name" + "\t" + "percent_coverage" + "\t" +  "value" + "\t" +  "total_number_peptides" + "\t" +  "total_number_distinct_peptides" + "\t" +  "unique_stripped_peptides" + "\t" +  "protein_description" + "\n")

#print("probability" + "\t" + "protein_name" + "\t" + "percent_coverage" + "\t" +  "value" + "\t" +  "total_number_peptides" + "\t" +  "total_number_distinct_peptides" + "\t" +  "unique_stripped_peptides" + "\t" +  "protein_description" + "\n")

for n1 in doc.getElementsByTagName("protein"):
	probability = n1.getAttribute("probability")
	protein_name = n1.getAttribute("protein_name")
	percent_coverage = n1.getAttribute("percent_coverage")
	total_number_peptides = n1.getAttribute("total_number_peptides")
	total_number_distinct_peptides = n1.getAttribute("total_number_distinct_peptides")
	unique_stripped_peptides = n1.getAttribute("unique_stripped_peptides")
		
	for n2 in n1.childNodes:
		if n2.nodeName == "parameter":
			value = n2.getAttribute("value")
		if n2.nodeName == "annotation":
			protein_description = n2.getAttribute("protein_description")
		
	with open(outfile_name, "a") as out_file:
		out_file.write(probability + "\t" + protein_name + "\t" + percent_coverage + "\t" +  value + "\t" +  total_number_peptides + "\t" +  total_number_distinct_peptides + "\t" +  unique_stripped_peptides + "\t" +  protein_description + "\n")
#	print(probability + "\t" + protein_name + "\t" + percent_coverage + "\t" +  value + "\t" +  total_number_peptides + "\t" +  total_number_distinct_peptides + "\t" +  unique_stripped_peptides + "\t" +  protein_description + "\n")

print("\n")
print("tsv file: " + outfile_name + "\n")
print("Ready!\n")
print("************************************************************")
