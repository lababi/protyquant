#########################################################################################################
# This script is just for convenience and to assure correct naming of the output files for subsequent analyses
# 2018-06-08, PIPQ analysis, Robert Winkler <robert.winkler@cinvestav.mx>
# License: GNU GPL v3
#########################################################################################################

import sys
import os

def print_usage():
	print("usage:")
	print("\tpython3 pipq_analysis.py input_pipq_file")

if len(sys.argv) != 2:
	print_usage()
	exit(1)

infile_name = str(sys.argv[1])

outfile_pi_name = infile_name.replace(".g",".pi")
outfile_pq_name = infile_name.replace(".g",".pq")

pipq_method = "-a"

#For debugging only
print("in:" + infile_name)
print("out pi:" + outfile_pi_name)
print("out pq:" + outfile_pq_name)


print("************************************************************\n")
print("PIPQ analysis: " + infile_name + "\n")

command_string = str("PIPQ.x64 " + pipq_method + " " + infile_name + " " + outfile_pi_name + " " + outfile_pq_name)

#For debugging only
#print("command: " + command_string)
os.system(command_string)

print("PIPQ result files: \n")
print("Protein interference:\t" + outfile_pi_name + "\n")
print("Protein quantification:\t" + outfile_pq_name +  "\n")
print("Ready!\n")
print("************************************************************")
