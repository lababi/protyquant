/* ScoreConvert.h */

/***********************************************************************
* Original version created on Oct 7, 2012:
* https://code.google.com/archive/p/protein-inference/
* see He Z, Huang T, Liu X, Zhu P, Teng B, Deng S. Protein inference: A protein
* quantification perspective. Comput Biol Chem. 2016 Aug;63:21-29. doi:
* 10.1016/j.compbiolchem.2016.02.006. Epub 2016 Feb 13. PubMed PMID: 26935399.
* License: GNU GPL v3
***********************************************************************/

#ifndef SCORECONVERT_H_INCLUDED
#define SCORECONVERT_H_INCLUDED

#include <cstdio>
#include <cstring>
#include <string>
#include <map>
#include <set>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <queue>
#include <stack>

using namespace std;

const double conv_diff = 1e-2;

class score_convert
{
private:

    vector<string> pt_name;
    vector<double> pt_sc;
    vector<double> pt_pb;

    vector<double> target;
    double A,B,pre_A,pre_B;

    vector<double> pp;

    int N;

    double log_(double x)
    {
        return log(x);
    }

    double exp_(double n)
    {
        return exp(n);
    }

    void import_score(map<string,double> & res_score)
    {
        pt_name.clear();
        pt_sc.clear();

        int k = 0;

        map<string,double>::iterator it;
        for(it = res_score.begin();it != res_score.end();it++)
        {
            pt_name.push_back(it->first);
            pt_sc.push_back(it->second);
            k++;
        }
        N = k;
    }

    bool EM_is_convergence()
    {
        double det = max(fabs(A - pre_A),fabs(B - pre_B));

        if(det < conv_diff) return true;
        return false;
    }

    void EM_Initialization()
    {
        srand(time(0));

        //EM algorithm initial parameter
        A = -1;
        B = 5;


        target.clear();
        for(int i = 0;i < N;i++)
            target.push_back(0);
    }

    void EM_E_step()
    {
        for(int i = 0;i < N; i++)
        {
            if(A * pt_sc[i] + B <= 0) target[i] = 1;
            else target[i] = 0;
        }
    }

    void EM_M_step()
    {
        double diff,scale,t,oldA,oldB,err,p,det;
        double prior1 = 0;
        double prior0 = 0;
        int i;

        for(i = 0;i < N; i++)
            if(target[i] == 0) prior0++;
            else prior1++;

        A = 0;
        B = log_((prior0 + 1.0) / (prior1 + 1.0));

        double hiTarget = (prior1 + 1.0) / (prior1 + 2.0);
        double loTarget = 1.0 / (prior0 + 2.0);
        double lambda = 1e-3;
        double olderr = 1e300;

        pp.clear();

        double pp_init_value = (prior1 + 1.0) / (prior0 + prior1 + 2.0);
        for(i = 0;i < N; i++)
            pp.push_back(pp_init_value);

        double count = 0;

        for(int it = 0;it <= 100; it++)
        {
            double a,b,c,d,e;
            a = b = c = d = e = 0;
            for(i = 0;i < N; i++)
            {
                if(target[i] == 1)
                    t = hiTarget;
                else
                    t = loTarget;

                double d1 = pp[i] - t;
                double d2 = pp[i] * (1 - pp[i]);

                a += pt_sc[i] * pt_sc[i] * d2;
                b += d2;
                c += pt_sc[i] * d2;
                d += pt_sc[i] * d1;
                e += d1;
            }

            if(fabs(d) < 1e-9 && fabs(e) < 1e-9)
                break;
            oldA = A;
            oldB = B;
            err = 0;

            while(1)
            {
                det = (a + lambda) * (b + lambda) - c * c;
                if(det == 0)
                {
                    lambda *= 10;
                    continue;
                }

                A = oldA + ((b + lambda) * d - c * e) / det;
                B = oldB + ((a + lambda) * e - c * d) / det;

                err = 0;

                for(i = 0;i < N; i++)
                {

                    if(target[i])
                        t = hiTarget;
                    else
                        t = loTarget;

                    p = 1.0 / (1.0 + exp_(pt_sc[i] * A + B));
                    pp[i] = p;

                    double log_p1,log_p2;

                    if(p == 0.0)
                        log_p1 = -200;
                    else
                        log_p1 = log_(p);

                    if(p == 1.0)
                        log_p2 = -200;
                    else
                        log_p2 = log_(1 - p);


                    err -= t*log_p1 + (1 - t) * log_p2;
                }

                if(err < olderr * (1 + 1e-7))
                {
                    lambda *= 0.1;
                    break;
                }
                lambda *= 10;
                if(lambda >= 1e6)
                    break;
            }
            diff = err - olderr;
            scale = 0.5 * (err + olderr + 1);

            if((diff > -1e-3*scale) && (diff < 1e-7*scale))
                count++;
            else
                count = 0;

            olderr = err;

            if(count == 3)
                break;
        }
    }


    void EM_algorithm()
    {
        int s = 0;
        EM_Initialization();
        //cout << "init value: A = " << A << ", B = " << B << endl;
        //cout << "N = " << N << endl;
        do
        {
            pre_A = A;
            pre_B = B;

            EM_E_step();

        //    cout << "\tE-step complete!" << endl;
            EM_M_step();
        //    cout << "\tM-step complete!" << endl;
            s = s + 1;

            //A=-A;
            //B=-B;

            //if(s > 2000) break;
            //cerr << "iterator " <<  s <<  " times." << " A = " << A << "\tB = " << B << endl;
        }
        while(!EM_is_convergence());
    }


    void cal_prob()
    {
        int i;
        pt_pb.clear();
        for(i = 0;i < N; i++)
        {
            double pb = 1.0 / (1.0 + exp_(A * pt_sc[i] + B));
            pt_pb.push_back(pb);
        }
    }

    void export_prob(map<string,double> & data)
    {
        int i;
        data.clear();
        for(i = 0;i < N; i++)
        {
            data[pt_name[i]] = pt_pb[i];
        }
    }

public:

    score_convert()
    {

    }

    map<string,double> get_prob(map<string,double> input_data)
    {
        map<string,double> output_data;
        import_score(input_data);
        EM_algorithm();
        cal_prob();
        export_prob(output_data);
        return output_data;
    }
};

#endif // SCORECONVERT_H_INCLUDED
