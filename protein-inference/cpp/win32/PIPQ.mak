GLPK_FLAG = \
/GL \
/I "..\glpk-4.65\src" \
/I "..\glpk-4.65\src\env" \
/I "..\glpk-4.65\src\misc" \
/I "..\glpk-4.65\src\draft" \
/I "..\glpk-4.65\src\minisat" \
/I "..\glpk-4.65\src\npp" \
/I "..\glpk-4.65\src\mpl" \
/I "..\glpk-4.65\src\bflib" \
/I "..\glpk-4.65\src\amd" \
/I "..\glpk-4.65\src\api" \
/I "..\glpk-4.65\src\simplex" \
/I "..\glpk-4.65\src\colamd" \
/I "..\glpk-4.65\src\zlib" \
/D "WIN32" /D "NDEBUG" /D "_CRT_SECURE_NO_WARNINGS" /D "_UNICODE" /D "UNICODE" \
/FD /EHsc /MT /Fd"Intermediate\vc90.pdb" \
/W3 /nologo /c /TC /errorReport:prompt


OBJSET = \
.\Intermediate\amd_1.obj \
.\Intermediate\amd_2.obj \
.\Intermediate\amd_aat.obj \
.\Intermediate\amd_control.obj \
.\Intermediate\amd_defaults.obj \
.\Intermediate\amd_dump.obj \
.\Intermediate\amd_info.obj \
.\Intermediate\amd_order.obj \
.\Intermediate\amd_post_tree.obj \
.\Intermediate\amd_postorder.obj \
.\Intermediate\amd_preprocess.obj \
.\Intermediate\amd_valid.obj \
.\Intermediate\advbas.obj \
.\Intermediate\asnhall.obj \
.\Intermediate\asnlp.obj \
.\Intermediate\asnokalg.obj \
.\Intermediate\ckasn.obj \
.\Intermediate\ckcnf.obj \
.\Intermediate\cplex.obj \
.\Intermediate\cpp.obj \
.\Intermediate\cpxbas.obj \
.\Intermediate\graph.obj \
.\Intermediate\gridgen.obj \
.\Intermediate\intfeas1.obj \
.\Intermediate\maxffalg.obj \
.\Intermediate\maxflp.obj \
.\Intermediate\mcflp.obj \
.\Intermediate\mcfokalg.obj \
.\Intermediate\mcfrelax.obj \
.\Intermediate\minisat1.obj \
.\Intermediate\mpl.obj \
.\Intermediate\mps.obj \
.\Intermediate\netgen.obj \
.\Intermediate\npp.obj \
.\Intermediate\pript.obj \
.\Intermediate\prmip.obj \
.\Intermediate\prob1.obj \
.\Intermediate\prob2.obj \
.\Intermediate\prob3.obj \
.\Intermediate\prob4.obj \
.\Intermediate\prob5.obj \
.\Intermediate\prrngs.obj \
.\Intermediate\prsol.obj \
.\Intermediate\rdasn.obj \
.\Intermediate\rdcc.obj \
.\Intermediate\rdcnf.obj \
.\Intermediate\rdipt.obj \
.\Intermediate\rdmaxf.obj \
.\Intermediate\rdmcf.obj \
.\Intermediate\rdmip.obj \
.\Intermediate\rdprob.obj \
.\Intermediate\rdsol.obj \
.\Intermediate\rmfgen.obj \
.\Intermediate\strong.obj \
.\Intermediate\topsort.obj \
.\Intermediate\weak.obj \
.\Intermediate\wcliqex.obj \
.\Intermediate\wrasn.obj \
.\Intermediate\wrcc.obj \
.\Intermediate\wrcnf.obj \
.\Intermediate\wript.obj \
.\Intermediate\wrmaxf.obj \
.\Intermediate\wrmcf.obj \
.\Intermediate\wrmip.obj \
.\Intermediate\wrprob.obj \
.\Intermediate\wrsol.obj \
.\Intermediate\btf.obj \
.\Intermediate\btfint.obj \
.\Intermediate\fhv.obj \
.\Intermediate\fhvint.obj \
.\Intermediate\ifu.obj \
.\Intermediate\luf.obj \
.\Intermediate\lufint.obj \
.\Intermediate\scf.obj \
.\Intermediate\scfint.obj \
.\Intermediate\sgf.obj \
.\Intermediate\sva.obj \
.\Intermediate\colamd.obj \
.\Intermediate\bfd.obj \
.\Intermediate\bfx.obj \
.\Intermediate\glpapi06.obj \
.\Intermediate\glpapi07.obj \
.\Intermediate\glpapi08.obj \
.\Intermediate\glpapi09.obj \
.\Intermediate\glpapi10.obj \
.\Intermediate\glpapi12.obj \
.\Intermediate\glpapi13.obj \
.\Intermediate\glphbm.obj \
.\Intermediate\glpios01.obj \
.\Intermediate\glpios02.obj \
.\Intermediate\glpios03.obj \
.\Intermediate\glpios07.obj \
.\Intermediate\glpios09.obj \
.\Intermediate\glpios11.obj \
.\Intermediate\glpios12.obj \
.\Intermediate\glpipm.obj \
.\Intermediate\glpmat.obj \
.\Intermediate\glprgr.obj \
.\Intermediate\glpscl.obj \
.\Intermediate\glpspm.obj \
.\Intermediate\glpssx01.obj \
.\Intermediate\glpssx02.obj \
.\Intermediate\lux.obj \
.\Intermediate\alloc.obj \
.\Intermediate\dlsup.obj \
.\Intermediate\env.obj \
.\Intermediate\error.obj \
.\Intermediate\stdc.obj \
.\Intermediate\stdout.obj \
.\Intermediate\stream.obj \
.\Intermediate\time.obj \
.\Intermediate\tls.obj \
.\Intermediate\cfg.obj \
.\Intermediate\cfg1.obj \
.\Intermediate\cfg2.obj \
.\Intermediate\clqcut.obj \
.\Intermediate\covgen.obj \
.\Intermediate\fpump.obj \
.\Intermediate\gmicut.obj \
.\Intermediate\gmigen.obj \
.\Intermediate\mirgen.obj \
.\Intermediate\spv.obj \
.\Intermediate\minisat.obj \
.\Intermediate\avl.obj \
.\Intermediate\bignum.obj \
.\Intermediate\dimacs.obj \
.\Intermediate\dmp.obj \
.\Intermediate\ffalg.obj \
.\Intermediate\fp2rat.obj \
.\Intermediate\fvs.obj \
.\Intermediate\gcd.obj \
.\Intermediate\jd.obj \
.\Intermediate\keller.obj \
.\Intermediate\ks.obj \
.\Intermediate\mc13d.obj \
.\Intermediate\mc21a.obj \
.\Intermediate\mt1.obj \
.\Intermediate\mygmp.obj \
.\Intermediate\okalg.obj \
.\Intermediate\qmd.obj \
.\Intermediate\relax4.obj \
.\Intermediate\rng.obj \
.\Intermediate\rng1.obj \
.\Intermediate\round2n.obj \
.\Intermediate\str2int.obj \
.\Intermediate\str2num.obj \
.\Intermediate\strspx.obj \
.\Intermediate\strtrim.obj \
.\Intermediate\triang.obj \
.\Intermediate\wclique.obj \
.\Intermediate\wclique1.obj \
.\Intermediate\mpl1.obj \
.\Intermediate\mpl2.obj \
.\Intermediate\mpl3.obj \
.\Intermediate\mpl4.obj \
.\Intermediate\mpl5.obj \
.\Intermediate\mpl6.obj \
.\Intermediate\mplsql.obj \
.\Intermediate\npp1.obj \
.\Intermediate\npp2.obj \
.\Intermediate\npp3.obj \
.\Intermediate\npp4.obj \
.\Intermediate\npp5.obj \
.\Intermediate\npp6.obj \
.\intermediate\proxy.obj \
.\intermediate\proxy1.obj \
.\Intermediate\spxat.obj \
.\Intermediate\spxchuzc.obj \
.\Intermediate\spxchuzr.obj \
.\Intermediate\spxlp.obj \
.\Intermediate\spxnt.obj \
.\Intermediate\spxprim.obj \
.\Intermediate\spxprob.obj \
.\Intermediate\spychuzc.obj \
.\Intermediate\spychuzr.obj \
.\Intermediate\spydual.obj \
.\Intermediate\adler32.obj \
.\Intermediate\compress.obj \
.\Intermediate\crc32.obj \
.\Intermediate\deflate.obj \
.\Intermediate\gzclose.obj \
.\Intermediate\gzlib.obj \
.\Intermediate\gzread.obj \
.\Intermediate\gzwrite.obj \
.\Intermediate\inffast.obj \
.\Intermediate\inflate.obj \
.\Intermediate\inftrees.obj \
.\Intermediate\trees.obj \
.\Intermediate\uncompr.obj \
.\Intermediate\zio.obj \
.\Intermediate\zutil.obj


{..\glpk-4.65\src\amd}.c{.\Intermediate}.obj:
	cl.exe $(GLPK_FLAG) /Fo.\Intermediate\$(@B).obj $<

{..\glpk-4.65\src\api}.c{.\Intermediate}.obj:
	cl.exe $(GLPK_FLAG) /Fo.\Intermediate\$(@B).obj $<

{..\glpk-4.65\src\bflib}.c{.\Intermediate}.obj:
	cl.exe $(GLPK_FLAG) /Fo.\Intermediate\$(@B).obj $<

{..\glpk-4.65\src\colamd}.c{.\Intermediate}.obj:
	cl.exe $(GLPK_FLAG) /Fo.\Intermediate\$(@B).obj $<

{..\glpk-4.65\src\draft}.c{.\Intermediate}.obj:
	cl.exe $(GLPK_FLAG) /Fo.\Intermediate\$(@B).obj $<

{..\glpk-4.65\src\env}.c{.\Intermediate}.obj:
	cl.exe $(GLPK_FLAG) /Fo.\Intermediate\$(@B).obj $<

{..\glpk-4.65\src\intopt}.c{.\Intermediate}.obj:
	cl.exe $(GLPK_FLAG) /Fo.\Intermediate\$(@B).obj $<

{..\glpk-4.65\src\minisat}.c{.\Intermediate}.obj:
	cl.exe $(GLPK_FLAG) /Fo.\Intermediate\$(@B).obj $<

{..\glpk-4.65\src\misc}.c{.\Intermediate}.obj:
	cl.exe $(GLPK_FLAG) /Fo.\Intermediate\$(@B).obj $<

{..\glpk-4.65\src\mpl}.c{.\Intermediate}.obj:
	cl.exe $(GLPK_FLAG) /Fo.\Intermediate\$(@B).obj $<

{..\glpk-4.65\src\npp}.c{.\Intermediate}.obj:
	cl.exe $(GLPK_FLAG) /Fo.\Intermediate\$(@B).obj $<

{..\glpk-4.65\src\proxy}.c{.\Intermediate}.obj:
	cl.exe $(GLPK_FLAG) /Fo.\Intermediate\$(@B).obj $<

{..\glpk-4.65\src\simplex}.c{.\Intermediate}.obj:
	cl.exe $(GLPK_FLAG) /Fo.\Intermediate\$(@B).obj $<

{..\glpk-4.65\src\zlib}.c{.\Intermediate}.obj:
	cl.exe $(GLPK_FLAG) /Fo.\Intermediate\$(@B).obj $<



PIPQFLAG = \
/O2 \
/Oi \
/GL \
/D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_UNICODE" /D "UNICODE" \
/FD /EHsc /MT /Gy /Fd"Intermediate\vc90.pdb" /W3 /nologo /c /Zi /TP /errorReport:prompt


PIPQLINK = \
/OUT:"PIPQ.exe" /INCREMENTAL:NO /NOLOGO \
/MANIFEST /MANIFESTFILE:".\Intermediate\PIPQ.exe.intermediate.manifest" \
/MANIFESTUAC:"level='asInvoker' uiAccess='false'" \
/SUBSYSTEM:CONSOLE /OPT:REF /OPT:ICF \
/LTCG /DYNAMICBASE /NXCOMPAT /MACHINE:X86 /ERRORREPORT:PROMPT \
kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib \
shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib


all: glpk.lib  PIPQ.exe


glpk.lib: $(OBJSET)
	lib.exe /out:glpk.lib .\Intermediate\*.obj


PIPQ.exe: glpk.lib
    cl.exe $(PIPQFLAG) /Fo.\Intermediate\main.obj ..\main.cpp
	link.exe $(PIPQLINK) /out:PIPQ.exe .\Intermediate\main.obj glpk.lib
