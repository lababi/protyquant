/* ProteinLP.h */

/***********************************************************************
* Original version created on Oct 7, 2012:
* https://code.google.com/archive/p/protein-inference/
* see He Z, Huang T, Liu X, Zhu P, Teng B, Deng S. Protein inference: A protein
* quantification perspective. Comput Biol Chem. 2016 Aug;63:21-29. doi:
* 10.1016/j.compbiolchem.2016.02.006. Epub 2016 Feb 13. PubMed PMID: 26935399.
* License: GNU GPL v3
***********************************************************************/


#ifndef PROTEINLP_H_INCLUDED
#define PROTEINLP_H_INCLUDED

#include <iostream>
#include <cstdio>
#include <string>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <map>
#include <set>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <queue>
#include <stack>
#include "glpk.h"
using namespace std;

const double INF = 1e100;

string conv_int2string(int x)
{
	 string res = "";
	 while(x) { res += char('0' + (x % 10)); x /= 10;}
	 int i,n = res.size(),k;
	 k = n / 2;
	 for(i = 0 ;i < k;i++) swap(res[i],res[n - i - 1]);
	 return res;
}

class ProteinLP
{
private:
    map<string,set<string> > protein_gourps;//protein groups

    vector<peptide_protein> psmprot_to_pepprot(const vector<psm_protein> & psm_prot_set)
    {
        vector<peptide_protein> pep_prot_set;
        map<string,int> pep_index;
        int i,j,n,m,k;

        n = psm_prot_set.size();
        k = 0;
        for(i = 0;i < n;i++)
        {
            const psm_protein * p = & psm_prot_set[i];
            if(pep_index.find(p->psm) == pep_index.end())
            {
                peptide_protein pp;
                pp.pep = p->psm;
                pp.score = p->score;
                m = p->rpts.size();
                for(j = 0;j < m;j++)
                    pp.rpts.insert(p->rpts[j]);
                pep_prot_set.push_back(pp);
                pep_index[p->psm] = k++;
            }
            else
            {
                int id = pep_index[p->psm];
                pep_prot_set[id].score += p->score;
                m = p->rpts.size();
                for(j = 0;j < m;j++)
                    pep_prot_set[id].rpts.insert(p->rpts[j]);
            }
        }
        return pep_prot_set;
    }

    vector<peptide_protein_groups> peppt_to_ptgp(const vector<peptide_protein> & pep_prot_set)
    {
        map<string,string> pt_gp;//protein -- peptide string


        int i,n;

        n = pep_prot_set.size();
        for(i = 0;i < n;i++)
        {
            const peptide_protein * p = & pep_prot_set[i];
            set<string>::iterator it_s;
            for(it_s = p->rpts.begin();it_s != p->rpts.end();it_s++)
            {
                if(pt_gp.find(*it_s) == pt_gp.end())
                    pt_gp[*it_s] = p->pep;
                else
                    pt_gp[*it_s] += p->pep;
            }
        }

        //genarate the map: protein groups name -> proteins
        map<string,string>::iterator it_m;
        for(it_m = pt_gp.begin();it_m != pt_gp.end();it_m++)
            protein_gourps[it_m->second].insert(it_m->first);

        vector<peptide_protein_groups> res;


        for(i = 0;i < n;i++)
        {
            const peptide_protein * p = & pep_prot_set[i];
            set<string>::iterator it_s;

            peptide_protein_groups ppg;
            ppg.pep = p->pep;
            ppg.score = p->score;

            for(it_s = p->rpts.begin();it_s != p->rpts.end();it_s++)
                ppg.rpts.insert(pt_gp[*it_s]);
            res.push_back(ppg);
        }
        return res;
    }

    map<string,double> lp_algorithm(const vector<peptide_protein_groups> & pep_ptgp_set)
    {
        int n,m,i,j,k;
        vector<int> ia,ja;
        vector<double> ar;
        map<string,int> proteins;
        map<string,double> res;

        //find out the number of d except the zero one
        n = pep_ptgp_set.size();
        m = 0;
        k = 0;
        for(i = 0;i < n;i++)
        {
            int sz = pep_ptgp_set[i].rpts.size();
            m += sz;

            set<string>::iterator itj;
            for(itj = pep_ptgp_set[i].rpts.begin();itj != pep_ptgp_set[i].rpts.end();itj++)
            {
                if(proteins.find(*itj) == proteins.end())
                    proteins[*itj] = ++k;
            }
        }

        glp_prob * lp;
        lp = glp_create_prob();
        glp_set_prob_name(lp,"ProteinLP");
        glp_set_obj_dir(lp,GLP_MIN);
        glp_add_rows(lp,m + n);

        int rn,cn;

        rn = 1;
        //add djk - tk <= 0
        for(i = 0;i < n;i++)
        {
            int tn = pep_ptgp_set[i].rpts.size();
            for(j = 0;j < tn;j++)
            {
                string row_name = "y";
                row_name += conv_int2string(rn);
                glp_set_row_name(lp,rn,row_name.c_str());
                glp_set_row_bnds(lp,rn,GLP_UP,-INF,0.0);
                rn++;
            }
        }

        //add bj - sum(djk) = 0;
        for(i = 0;i < n;i++)
        {
            string row_name = "y";
            row_name += conv_int2string(rn);
            glp_set_row_name(lp,rn,row_name.c_str());
            glp_set_row_bnds(lp,rn,GLP_FX,pep_ptgp_set[i].score,pep_ptgp_set[i].score);
            rn++;
        }

        //objective function z=a1x1+a2x2 ...
        glp_add_cols(lp,k + m);
        cn = 1;

        for(i = 0;i < k;i++)
        {
            string col_name = "t";
            col_name += conv_int2string(i + 1);
            glp_set_col_name(lp,cn,col_name.c_str());
            glp_set_col_bnds(lp,cn,GLP_LO,0.0,INF);
            glp_set_obj_coef(lp,cn,1.0);
            cn++;
        }
        for(i = 0;i < n;i++)
        {
            int sz = pep_ptgp_set[i].rpts.size();
            for(j = 0;j < sz;j++)
            {
                string col_name = "d";
                col_name += conv_int2string(i + 1);
                col_name += ",";
                col_name += conv_int2string(j + 1);
                glp_set_col_name(lp,cn,col_name.c_str());
                glp_set_col_bnds(lp,cn,GLP_LO,0.0,INF);
                glp_set_obj_coef(lp,cn,0.0);
                cn++;
            }
        }
        rn = 1;
        cn = k + 1;
        for(i = 0;i < n;i++)
        {
            set<string>::iterator itj;
            for(itj = pep_ptgp_set[i].rpts.begin();itj != pep_ptgp_set[i].rpts.end();itj++)
            {
                ia.push_back(rn);
                ja.push_back(cn);
                ar.push_back(1);

                ia.push_back(rn);
                ja.push_back(proteins[*itj]);
                ar.push_back(-1);

                rn++,cn++;
            }
        }

        cn = k + 1;
        for(i = 0;i < n;i++)
        {
            int sz = pep_ptgp_set[i].rpts.size();
            for(j = 0;j < sz;j++)
            {
                ia.push_back(rn);
                ja.push_back(cn);
                ar.push_back(1);
                cn++;
            }
            rn++;
        }

        int * ia_ = new int[ia.size() + 1];
        int * ja_ = new int[ja.size() + 1];
        double * ar_ = new double[ar.size() + 1];

        for(i = 0;i < (int)ia.size();i++)
            ia_[i+1] = ia[i],ja_[i+1] = ja[i],ar_[i + 1] = ar[i];
        glp_load_matrix(lp,ia.size(),ia_,ja_,ar_);
        glp_simplex(lp,NULL);

        cn = k + 1;
        for(i = 0;i < n;i++)
        {
            set<string>::iterator itj;
            const set<string> * p = & pep_ptgp_set[i].rpts;
            for(itj = p->begin();itj != p->end();itj++)
            {
               if(res.find(*itj) == res.end())
               {
                   res[*itj] = glp_get_col_prim(lp,cn);
               }
               else
               {
                   res[*itj] += glp_get_col_prim(lp,cn);
               }
               cn++;
            }
        }

        //ofstream fout("res.txt");

        map<string,double>::iterator itk;
        vector<protein_score> pts;

        for(itk = res.begin();itk != res.end(); itk++)
        {
            set<string>::iterator it_s;
            const set<string> * p = & protein_gourps[itk->first];
            for(it_s = p->begin();it_s != p->end();it_s++)
               pts.push_back(protein_score(*it_s,itk->second));
        }

        sort(pts.begin(),pts.end());

        map<string,double> ans;

        for(i = 0;i < (int) pts.size();i++)
        {
            ans[pts[i].name] = pts[i].score;
            //fout << pts[i].name << "," << pts[i].score << "," << endl;
        }

        glp_delete_prob(lp);

        delete [] ia_;
        delete [] ja_;
        delete [] ar_;

        return ans;
    }

public:

    ProteinLP()
    {

    }

    map<string,double> get_result(const vector<psm_protein> & psm_pt)
    {
        vector<peptide_protein> pep_pt = psmprot_to_pepprot(psm_pt);
        vector<peptide_protein_groups> pep_ptgp = peppt_to_ptgp(pep_pt);
        map<string,double> res = lp_algorithm(pep_ptgp);
        return res;
    }
};

#endif // PROTEINLP_H_INCLUDED
