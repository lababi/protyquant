/* base_class.h */

/***********************************************************************
* Original version created on Oct 7, 2012:
* https://code.google.com/archive/p/protein-inference/
* see He Z, Huang T, Liu X, Zhu P, Teng B, Deng S. Protein inference: A protein
* quantification perspective. Comput Biol Chem. 2016 Aug;63:21-29. doi:
* 10.1016/j.compbiolchem.2016.02.006. Epub 2016 Feb 13. PubMed PMID: 26935399.
* 2018-06-07, struct protein_quantity added by Robert Winkler <robert.winkler@cinvestav.mx>
* License: GNU GPL v3
***********************************************************************/

#ifndef BASE_CLASS_H_INCLUDED
#define BASE_CLASS_H_INCLUDED

struct psm_protein
{
    string psm;
    double score;
    vector<string> rpts;//related proteins

    void clear()
    {
        psm = "";
        score = 0;
        rpts.clear();
    }
};

struct protein_score
{
    string name;
    double score;

    protein_score(string name_,double score_)
    {
        name = name_;
        score = score_;
    }

    bool operator < (const protein_score & r) const
    {
        return score > r.score;
    }
};

struct protein_quantity
{
    string name;
    double quantity;

    protein_quantity(string name_,double quantity_)
    {
        name = name_;
        quantity = quantity_;
    }

    bool operator < (const protein_quantity & r) const
    {
        return quantity > r.quantity;
    }
};


struct peptide_protein
{
    string pep;
    double score;
    set<string> rpts;

    void clear()
    {
        pep = "";
        score = 0;
        rpts.clear();
    }

    bool operator < (const peptide_protein & r) const
    {
        return score > r.score;
    }
};

struct peptide_protein_groups
{
    string pep;
    double score;

    set<string> rpts;
};

#endif // BASE_CLASS_H_INCLUDED
