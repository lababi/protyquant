/* main.cpp */

/***********************************************************************
* Original version created on Oct 7, 2012:
* https://code.google.com/archive/p/protein-inference/
* see He Z, Huang T, Liu X, Zhu P, Teng B, Deng S. Protein inference: A protein
* quantification perspective. Comput Biol Chem. 2016 Aug;63:21-29. doi:
* 10.1016/j.compbiolchem.2016.02.006. Epub 2016 Feb 13. PubMed PMID: 26935399.
* 2018-06-07, output of file with protein quantites added by Robert Winkler <robert.winkler@cinvestav.mx>
* License: GNU GPL v3
***********************************************************************/

#include <cstdio>
#include <cstring>
#include <string>
#include <map>
#include <set>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <queue>
#include <stack>
#include "ScoreConvert.h"
#include "base_class.h"
#include "ProteinLP.h"

using namespace std;

//read from graph file
vector<psm_protein> read_file(char * file_name)
{
    ifstream fin(file_name);

    vector<psm_protein> psm;

    psm_protein temp;

    int k = 0;
    char type[2];

    psm.clear();//clear the old file data

    while(fin >> type)
    {
        if(type[0] == 'e')
        {
            k++;
            temp.clear();
            fin >> temp.psm;
        }
        else if(type[0] == 'r')
        {
            string pt;
            fin >> pt;
            temp.rpts.push_back(pt);
        }
        else if(type[0] == 'p')
        {
            fin >> temp.score;
            psm.push_back(temp);
        }
    }
    return psm;
}

map<string,double> psm_avg_algorithm(const vector<psm_protein> & psm)
{
    int n,m,i,j;
    map<string,double> res_avg;

    n = psm.size();

    res_avg.clear();//clear the avg result

    for(i = 0;i < n;i++)
    {
        psm_protein temp = psm[i];

        m = temp.rpts.size();

        for(j = 0;j < m;j++)
        {
            if(res_avg.find(temp.rpts[j]) == res_avg.end())
            {
                res_avg[temp.rpts[j]] = temp.score / m;
            }
            else
            {
                res_avg[temp.rpts[j]] += temp.score / m;
            }
        }
    }
    return res_avg;
}

map<string,double> psm_sum_algorithm(const vector<psm_protein> & psm)
{
    int n,m,i,j;
    map<string,double> res_sum;

    n = psm.size();

    res_sum.clear();//clear the sum result

    for(i = 0;i < n;i++)
    {
        psm_protein temp = psm[i];

        m = temp.rpts.size();

        for(j = 0;j < m;j++)
        {
            if(res_sum.find(temp.rpts[j]) == res_sum.end())
            {
                res_sum[temp.rpts[j]] = temp.score;
            }
            else
            {
                res_sum[temp.rpts[j]] += temp.score;
            }
        }
    }
    return res_sum;
}

void print_usage()
{
    cerr << "PIPQ, v20180614" << endl;
    cerr << "usage:" << endl;
    cerr << "\tPIPQ -option input_file output_file_PI output_file_PQ" << endl;
    cerr << "where option include:" << endl;
    cerr << "\t-s   Multiple Counting algorithm" << endl;
    cerr << "\t-a   Equal Division algorithm" << endl;
    cerr << "\t-lp  Linear Programming Model algorithm" << endl;
}
int main(int argv,char * argc[])
{
    if(argv != 5 || (strcmp(argc[1],"-s") != 0 && strcmp(argc[1],"-a") != 0 && strcmp(argc[1],"-lp") != 0))
    {
        print_usage();
        return 1;
    }

    vector<psm_protein> psm = read_file(argc[2]);

    map<string,double> res;

    if(strcmp(argc[1],"-s") == 0)
    {
        res = psm_sum_algorithm(psm);
    }
    else if(strcmp(argc[1],"-a") == 0)
    {
        res = psm_avg_algorithm(psm);
    }
    else if(strcmp(argc[1],"-lp") == 0)
    {
        ProteinLP lp_prob;
        res = lp_prob.get_result(psm);
    }

    score_convert s_c;

    map<string,double> pb = s_c.get_prob(res);

//sort protein inference scores and write into file

    map<string,double>::iterator it_m;


    vector<protein_score> ps;
    for(it_m = pb.begin();it_m != pb.end();it_m++)
        ps.push_back(protein_score(it_m->first,it_m->second));

    sort(ps.begin(),ps.end());

    ofstream fout1(argc[3]);
    for(int i = 0;i < (int) ps.size();i++)
        fout1 << ps[i].name << "\t" << ps[i].score << endl;

//sort weighted protein quantity and write into file

    vector<protein_quantity> pq;
    for(it_m = res.begin();it_m != res.end();it_m++)
    pq.push_back(protein_quantity(it_m->first,it_m->second));

    sort(pq.begin(),pq.end());

    ofstream fout2(argc[4]);
    for(int i = 0;i < (int) pq.size();i++)
        fout2 << pq[i].name << "\t" << pq[i].quantity << endl;

    return 0;
}
