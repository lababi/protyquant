# ProtyQuant

# Installing the Graphical User Interface (GUI) version of ProtyQuant

ProtyQuant requires two files in the same directory: The main program `protyquant.py` and the PIPQ binary.  
For Microsoft Windows and Linux, those files can be copied from the `python-gui` directory.
The program is started by:
~~~
python3 protyquant.py
~~~
ProtyQuant will check for the used operating system and use the correct PIPQ binary.

# Microsoft Windows Installer

The directory `windows_installer` contains a Microsoft Windows installer for ProtyQuant.

# Running with Docker

~~~
docker run -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix:rw -v /home/rob/dataspace:/data robertmp64/protyquant
~~~

If you cannot open a GUI as root ('Cannot connect to display!' error), you need to run `xhost +` first.

# GLPK (GNU Linear Programming Kit)

For cross-compiling the MS Windows .exe file on Linux, you have to compile the GLPK library similar to the instructions at:  
<https://www.tinc-vpn.org/examples/cross-compiling-64-bit-windows-binary/>

To build the 64-bit MinGW library use:
./configure --host=x86_64-w64-mingw32
make  
make install  

To build the Windows executables (32- or 64-bit), use the mak files in the /win32 and /win64 directories:

nmake /s /f PIPQ.mak
